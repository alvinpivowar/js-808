# Overview
This project was done as an exercise.  It is a good example of a full-stack Angular 5 / Node.js application.

* Angular 5 / CLI
* Font-Awesome
* Node.js
* Express

# Prerequisites
* `git`
* `@angular/cli`
* `Node.js`
* `npm`

# Building
    > npm install
    > npm run build
    > npm run start

You can then launch a browser:`http://localhost:3000`
The simulator will then render.

# REST
The front-end is completely data-driven by an Express-powered REST API. Whilst in the browser you can try the following:

`GET http://localhost:3000/api/sequences` 
Returns an array of "abbreviated" sequences.

`GET http://localhost:3000/api/sequences/NAME` 
Returns one full sequence by name.

`GET http://localhost:3000/api/voices` 
Returns an array of all of the defined voices.

_Next Steps: Add POST and PUT endpoints to support CRUD on sequences and voices.  Note, that there is a `version` property on a sequence; I was intended for non-destructive CRUD,_

# Front-End
Even though this is (literally) a one-page application, the front-end defines quite a few models and components:

### Models

**Sequence** 
A sequence represents the complete "song."  Besides it's identification properties, a sequence is a container for a `TimeSignature`, the number of beats per minute, and a collection of musical `Part`.

**TimeSignature** 
This object contains the upper and lower meter numerals as well as a method that calculates the synthesizer step duration given the number of beats per minute.

_Note: time signatures are completely implemented within the components, but are not currently exposed in the UI.
When fully implemented, the pattern bar will display the editable time signature on the left and vertical measure lines will be rendered. Since the pattern bar controls the paging, any changes in the meter will result in the number of visible steps to change so that only complete measures are seen._

**Part** 
Each musical part specifies the synthesizer voice to use, and the on/off values for the synthesizer steps.

**Voice** 
A voice is what sound to use.  Since this is a non-aural synthesizer, the only properties are the vloice's name, and what the on indicator color is.
If this was a "real" synthesizer, then this class would contain the properties for determining the sound. ex: Attack, Delay, Amplitude, Parametric Data, ...

**Pattern** 
A pattern is a first-class collection of synthesizer steps for a part.  It exposes behavior to add and remove steps as well as indicating what is the right-most significant step for a `Part`.

**PatternStep** 
This represents a single synthesizer step (for a `Part`) and whether it is on or off.

**Player** 
The player is the "owner" of the current sequence, and is a simple event handler for component coordination.  The `Player`also controls the `start()`/`stop()`/`step()` behavior.

**PlayerTime** 
This is used by the player to "run" the sequence.

_Note: The `step()` method is used by the `PlayerTimer` when it is time to have the synthesizer move between steps.  However, the `Player` is written so that from a stop state you can repeated call `step()`.  This was intended for a debug mode with single-stepping._

### Components

**synthesizer.module.ts** 
**js808-synthesizer.component.ts** 
All synthesizer components (and service) are in this module.  The synthesizer componet is the top-level container for the entire synthesizer.  It includes the button bar, the sequence bar, the pattern bar, and the parts collection.

**js808-button-bar.component.ts** 
Currently, this is completely empty. When the project supports full back-end CRUD, then the button bar would have sequence save, sequence load, version inspector, and voice manager. The voice manager would support voices CRUD for all sequences, as well as adding / removing defined voices for the current sequence.

**js808-sequence-bar.component.ts** 
The sequence bar is a container for the player controls and the sequence selector
_Note: When "loading" is moved to the button bar, the sequence selector can be removed, and the sequence bar will simply display the current sequence and version._

**js808-player-controls.component.ts** 
This contains the buttons that both show and control the play / stop player state.  It also controls the input for the beats per minute.

**js808-sequence-selector.component.ts** 
This is a temporary component for selecting one of the server's sequences.

**js808-pattern-bar.component.ts** 
This control renders the double line-delimited step indices.  It also controls the left/right paging of the steps; internally, there is no limit to the number of synthesizer steps.
This control is also where the debugging controls would reside.  The control would expand with breakpoint checkboxes below the indices and forward / backward step controls on the left.

_Note: Paging is completely implemented within the components, but is not currently exposed in the UI._

**js808-parts-collection.component.ts** 
Another container control; this time, it contains the array of musical parts.  A sequence can have any number of parts.

**js808-part.component.ts** 
This renders as the voice name on the left and a set of synthesizer steps on the right.  This component is an "observer" of the paging event and adjust it's steps accordingly.
Step editing is also handled by this component.
Most importantly, each separate part component "observes" step events and handles the voice animation.

### Service 
**sequence.service.ts** 
Exposes promise-wrapped HTTP requests that return first-class objects. In this way, consumers can utilize business logic without having to inject services or import helpers.

### CSS
The project uses SCSS.
Each component has it's own isolated (`:host`) styles.  Common styles are in `styles.scss`, mixins in `mixins.scss`, and synthesizer variables in `app/synthesizer/synthesizer.variables.scss`.
