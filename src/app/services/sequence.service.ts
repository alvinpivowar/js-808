import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"

import { Sequence } from "../models/Sequence";
import { Voice } from "../models/Voice"

@Injectable()
export class SequenceService {
  constructor(private http: HttpClient) {}

    public getAllSequences(): Promise<Array<Sequence>> {
        return new Promise<Array<Sequence>>((resolve, reject) => {
            this.http.get("/api/sequences")
                .toPromise()
                .then(response => resolve(Array.prototype.map.call(response, item => Sequence.fromJson(item))));
        })
    }

    public getSequenceByName(sequenceName): Promise<Sequence> {
        return new Promise<Sequence>((resolve, reject) => {
            this.http.get(`/api/sequences/${sequenceName}`)
                .toPromise()
                .then(response => resolve(Sequence.fromJson(response)));
        });
    }

    public getAllVoices(): Promise<Array<Voice>> {
        return new Promise<Array<Voice>>((resolve, reject) => {
            this.http.get("/api/voices")
                .toPromise()
                .then(response => resolve(Array.prototype.map.call(response, item => Voice.fromJson(item))));
        })
    }
}