import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SynthesizerModule } from "./synthesizer/synthesizer.module";

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SynthesizerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
