export class Voice {
    static fromJson(obj: any): Voice {
        let voice = new Voice(obj.name, obj.color);
        return voice;
    }

    constructor(
        public name: string,
        public color: string
    ) {}
}