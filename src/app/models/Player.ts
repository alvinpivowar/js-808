import { IPlayer } from "./IPlayer"
import { PlayerTimer } from "./PlayerTimer"
import { Sequence } from "./Sequence"

enum PlayerEventType {
    Paging,
    Sequence,
    Step
}

enum PlayerStateType {
    Playing,
    Stopped
}

class PlayerEvent {
    constructor(
        public type: PlayerEventType,
        public data: any
    ) {}
}

type HandlerFn = (event: PlayerEvent) => Boolean;

class PlayerEventHandler {
    constructor(
        public who: string,
        public handlerFn: HandlerFn
    ) {}
}

class PagingInfo {
    constructor(
        public pageIndex: number,
        public pageSize: number,
        public totalNumberOfItems: number
    ) {}
}

let playerEventRegistrations: Array<PlayerEventHandler> = [];

function raiseEvent(event: PlayerEvent) {
    playerEventRegistrations.forEach(registration => registration.handlerFn(event));
}

function unregisterPlayerEventHandler(who: string) {
    const index = playerEventRegistrations.findIndex(registration => registration.who === who);
    if (index !== -1)
        playerEventRegistrations.splice(index, 1);
}

class Player implements IPlayer {
    public get paging(): PagingInfo { return this._pagingInfo;}

    public set paging(value: PagingInfo) {
        this._pagingInfo = value;

        raiseEvent(new PlayerEvent(PlayerEventType.Paging, this._pagingInfo));
    }

    public get sequence(): Sequence { return this._sequence; }

    public set sequence(value: Sequence) { 
        if (!(this._sequence && this._sequence.sequenceName === value.sequenceName)) {
            this.stop();
            this._sequence = value;

            raiseEvent(new PlayerEvent(PlayerEventType.Sequence, this._sequence));
        }
    }

    public get state(): PlayerStateType { return this._state; }

    public get stepIndex(): number { return this._stepIndex; }
    
    constructor() {
        this._playerTimer = new PlayerTimer(this);
        this._state = PlayerStateType.Stopped;
    }

    public play() {
        this._playerTimer.stop();

        this._stepIndex = undefined;
        this._state = this._playerTimer.start() ? PlayerStateType.Playing : PlayerStateType.Stopped;

        if (this._state === PlayerStateType.Playing)
            raiseEvent(new PlayerEvent(PlayerEventType.Step, this._stepIndex));
    }

    public step(debugMode: Boolean = true) {
        if (debugMode)
            this._playerTimer.stop();

        this._state = PlayerStateType.Playing;
        this._stepIndex = typeof(this._stepIndex) !== "undefined" ? 1 + this._stepIndex : 0;

        raiseEvent(new PlayerEvent(PlayerEventType.Step, this._stepIndex));
    }

    public stop() {
        this._playerTimer.stop();
        
        this._state = PlayerStateType.Stopped;
        this._stepIndex = undefined;

        raiseEvent(new PlayerEvent(PlayerEventType.Step, this._stepIndex));
    }

    public registerEventHandler(who: string, handlerFn: HandlerFn): Function {
        playerEventRegistrations.push(new PlayerEventHandler(who, handlerFn));
        return unregisterPlayerEventHandler.bind(null, who);
    }

    private _pagingInfo: PagingInfo;
    private _playerTimer: PlayerTimer;
    private _sequence: Sequence;
    private _state: PlayerStateType;
    private _stepIndex: number;
}

export { PagingInfo, Player, PlayerEvent, PlayerEventType, PlayerStateType }