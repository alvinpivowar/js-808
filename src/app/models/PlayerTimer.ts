import { IPlayer } from "./IPlayer"
import { TimeSignature } from "./TimeSignature"

export class PlayerTimer {
    public startTicks: number;
    public stopTicks: number;
    public stepTickDuration: number;

    constructor(private player: IPlayer)
    {}

    public start(): Boolean {
        if (! this.player.sequence)
            return false;

        const lastStepIndex: number = this.player.sequence.getHighestSetPatternIndex();
        if (lastStepIndex === 0)
            return false;

        const timeSignature: TimeSignature  = this.player.sequence.timeSignature || new TimeSignature(4, 4);
        const beatsPerMinute: number = this.player.sequence.beatsPerMinute;
        if (!beatsPerMinute)
            return false;

        this.stepTickDuration = timeSignature.calculateStepDurationInMillesconds(beatsPerMinute);

        const intervalTicks = this.stepTickDuration / 10;
        this.startTicks = new Date().getTime();
        this.stopTicks = this.stepTickDuration * (1 + lastStepIndex) +this.startTicks;
        
        this.player.step(false);
        this._timerId = setInterval(this.tickHandler.bind(this), intervalTicks)

        return true;
    }

    public stop() {
        if (this._timerId) {
            clearInterval(this._timerId);
            this._timerId = undefined;
        }
    }

    private tickHandler() {
        const nowTicks: number = new Date().getTime();
        if (nowTicks > this.stopTicks) {
            this.player.stop();
            return;
        }

        const proposedStepIndex = Math.floor((nowTicks - this.startTicks) / this.stepTickDuration);
        if (proposedStepIndex > this.player.stepIndex)
            this.player.step(false)
    }

    private _timerId: number;
}