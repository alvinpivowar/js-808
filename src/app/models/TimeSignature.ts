export class TimeSignature {
    static fromJson(obj: any): TimeSignature {
        let timeSignature = new TimeSignature(obj.upperNumeral, obj.lowerNumeral);
        return timeSignature;
    }

    constructor(
        public upperNumeral: number,
        public lowerNumeral: number
    ) {}

    public calculateStepDurationInMillesconds(beatsPerMinute: number): number {
        const millisecondsPerMinute = 1000 * 60;
        const millisecondsPerBeat = millisecondsPerMinute / beatsPerMinute;
        const meterScaling = 4 / this.lowerNumeral;
        
        const stepDuration = meterScaling * millisecondsPerBeat;
        return stepDuration;
    }
}