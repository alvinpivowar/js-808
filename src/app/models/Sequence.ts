import { Part } from "./Part";
import { TimeSignature } from "./TimeSignature";

export class Sequence {
    static fromJson(obj: any): Sequence {
        let timeSignature = obj.timeSignature ? TimeSignature.fromJson(obj.timeSignature) : undefined;
        let parts = obj.parts ? Array.prototype.map.call(obj.parts, item => Part.fromJson(item)) : undefined

        let sequence =  new Sequence(
            obj.sequenceName,
            obj.authorName,
            obj.createDateTime,
            obj.version,
            obj.versionDateTime,

            timeSignature,
            obj.beatsPerMinute,
            parts
        );

        return sequence;
    }

    constructor(
        public sequenceName: string,
        public authorName: string,
        public createDateTime: Date,
        public version: number,
        public versionDateTime: Date,
    
        public timeSignature: TimeSignature,
        public beatsPerMinute: number,
        public parts: Array<Part>
    ) {}

    public getHighestPatternIndex(): number {
        if (!this.parts)
            return 0;

        return this.parts.reduce((previous: number, current: Part, index: number) =>
            Math.max(previous, current.pattern.highestIndex),
            0);
    }

    public getHighestSetPatternIndex(): number {
        if (!this.parts)
            return 0;
    
        let result = 0;
        this.parts.forEach(part => {
            part.pattern.steps.forEach((step, index) => {
                if (step.value && index > result)
                    result = index;
            });
        });
    
        return result;
    }
}