import { Pattern } from "./Pattern";
import { Voice } from "./Voice";

export class Part {
    static fromJson(obj: any): Part {
        let voice = Voice.fromJson(obj.voice);
        let pattern = Pattern.fromJson(obj.pattern);

        return new Part(voice, pattern);
    }

    constructor(
        public voice: Voice,
        public pattern: Pattern
    ) {}
}