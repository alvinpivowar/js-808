
class PatternStep {
    static fromJson(obj: any): PatternStep {
        let patternStep = new PatternStep(!!(obj && obj.value));
        return patternStep;
    }

    constructor(
        public value: Boolean
    ) {}
}

class Pattern {
    static fromJson(obj: any): Pattern {
        const steps = (obj && obj.steps)
            ? obj.steps
            : []

        while (steps.length < 20)
            steps.push({});
            
        let pattern = new Pattern(
            Array.prototype.map.call(steps, item => PatternStep.fromJson(item))
        );

        return pattern;
    }

    get highestIndex(): number { return this._highestIndex; }

    constructor(
        public steps: Array<PatternStep>
    ) {
        this.updateHighestIndex();
    }

    public addStep(step: PatternStep) {
        this.steps.push(step);
        this.updateHighestIndex();
    }

    public removeStep(index: number) {
        if (index < this.steps.length)
            this.steps.splice(index, 1);
    }

    private _highestIndex: number;
    private updateHighestIndex() {
        this._highestIndex = this.steps.reduce((previous: number, current: PatternStep, index: number) => index > previous ? index : previous, 0);
    }
}

export { Pattern, PatternStep };