import { Sequence } from "./Sequence"

export interface IPlayer {
    sequence: Sequence,
    stepIndex: number,

    step(debugMode: Boolean),
    stop()
}