import { Component, Input, OnDestroy, OnInit } from "@angular/core"

import { Player, PlayerEvent, PlayerEventType, PlayerStateType } from "../../models/Player"

const component_name = "js808-player-controls";
@Component({
  selector: component_name,
  templateUrl: "./js808-player-controls.component.html",
  styleUrls: ["./js808-player-controls.component.scss"]
})

export class Js808PlayerControlsComponent implements OnDestroy, OnInit {
  public PlayerStateType; 
  public sequence: any;

  @Input() player: Player;

  constructor() {
    this.PlayerStateType = PlayerStateType;
  }

  // comment out play and uncomment out step to see debug mode.
  public onPlay() {
    this.player.play();
    //this.player.step();
  }

  public onStop() {
    this.player.stop();
  }

  ngOnInit() {
    this.sequence = this.player.sequence || {};

    this._unregisterHandlerFn = this.player.registerEventHandler(component_name, this.handler.bind(this));
  }

  ngOnDestroy() {
    if (this._unregisterHandlerFn)
      this._unregisterHandlerFn();
  }
     
  private handler(event: PlayerEvent): Boolean {
    if (event.type === PlayerEventType.Sequence) {
      this.sequence = event.data;

      return true;
    }

    return false;
  }


  private _unregisterHandlerFn: Function;
}