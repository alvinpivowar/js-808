import { Component, EventEmitter, OnInit, Output } from "@angular/core"

import { Sequence } from "../../models/Sequence"
import { SequenceService } from "../../services/sequence.service"

@Component({
  selector: "js808-sequence-selector",
  templateUrl: "./js808-sequence-selector.component.html",
  styleUrls: ["./js808-sequence-selector.component.scss"]
})

export class Js808SequenceSelectorComponent implements OnInit {
  public allSequences: Array<Sequence>;
  public currentSequenceName: string;

  @Output() onSequenceChangeEvent = new EventEmitter<string>();
  
  constructor(
    private sequenceService: SequenceService
  ) {}

  public onSequenceChange() {
    this.onSequenceChangeEvent.emit(this.currentSequenceName);
  }

  ngOnInit() {
    this.sequenceService.getAllSequences().then(sequences => {
      this.allSequences = sequences;
      this.currentSequenceName = sequences[0].sequenceName;

      this.onSequenceChange();
    });
  }
}