import { Component, Input, OnDestroy, OnInit } from "@angular/core"

import { Part } from "../../models/Part"
import { PagingInfo, Player, PlayerEvent, PlayerEventType } from "../../models/Player"
import { PatternStep } from "../../models/Pattern"

const component_name: string = "js808-part";
@Component({
  selector: component_name,
  templateUrl: "./js808-part.component.html",
  styleUrls: ["./js808-part.component.scss"]
})

export class Js808PartComponent implements OnDestroy, OnInit {
  public steps: Array<PatternStep>;

  @Input() part: Part;
  @Input() player: Player;

  constructor() {}

  public toggleStepValue(step: PatternStep) {
    step.value = !step.value;
  }

  ngOnInit() {
    this._pagingInfo = this.player.paging;
    this.initializePaging();

    this._unregisterHandlerFn = this.player.registerEventHandler(component_name, this.handler.bind(this));
  }

  ngOnDestroy() {
    if (this._unregisterHandlerFn)
      this._unregisterHandlerFn();
  }

  private handler(event: PlayerEvent): Boolean {
    if (event.type === PlayerEventType.Paging) {
      this._pagingInfo = event.data;
      this.initializePaging();

      return true;
    }

    if (event.type == PlayerEventType.Step) {
      const activeStepIndex: number = event.data;
      this.steps.forEach((step: any) => step.$active = step.value && step.$index === activeStepIndex);
    }

    return false;
  }

  private initializePaging() {
    if (!this._pagingInfo)
      return;

    const sourceSteps = (this.part.pattern && this.part.pattern.steps) || [];
    const startIndex = this._pagingInfo.pageIndex * this._pagingInfo.pageSize;
    
    let result = [];

    for (let i = 0; i < this._pagingInfo.pageSize; ++i) {
      let index = i + startIndex;
      let sourceStep = index < sourceSteps.length ? sourceSteps[index] : undefined;
      let step: any = sourceStep || new PatternStep(false);
      step.$index = index;
      result.push(step);
    }

    this.steps = result;
  }

  private _pagingInfo: PagingInfo;
  private _unregisterHandlerFn: Function;
}