import { Component, Input, OnDestroy, OnInit } from "@angular/core"

import { PagingInfo, Player, PlayerEvent, PlayerEventType } from "../../models/Player"
import { Sequence } from "../../models/Sequence"

const component_name: string = "js808-pattern-bar";
const page_size: number = 20;

@Component({
  selector: component_name,
  templateUrl: "./js808-pattern-bar.component.html",
  styleUrls: ["./js808-pattern-bar.component.scss"]
})

export class Js808PatternBarComponent implements OnDestroy,OnInit {
  public boxes: Array<any>;
  public pageCount: number;
  public pageIndex: number;

  @Input() player: Player;

  constructor(
  ) {}

  ngOnInit() {
    this.initializePaging();

    this._unregisterHandlerFn = this.player.registerEventHandler(component_name, this.handler.bind(this));
  }

  ngOnDestroy() {
    if (this._unregisterHandlerFn)
      this._unregisterHandlerFn();
  }

  private handler(event: PlayerEvent): Boolean {
    if (event.type === PlayerEventType.Sequence) {
      this._sequence = event.data;
      this.initializePaging();

      return true;
    }

    if (event.type === PlayerEventType.Step) {
      const activeStep = event.data;
      const baseIndex = this.pageIndex * page_size;
      
      this.boxes.forEach((box: any, index) => {
          box.$active = baseIndex + index === activeStep;
      });

      return true;
    }

    return false;
  }

  private initializePaging() {
    let highestIndex: number = this._sequence
      ? this._sequence.getHighestPatternIndex()
      : page_size;

    this.pageIndex = 0;
    this.pageCount = Math.floor((highestIndex + (page_size - 1)) / page_size);
    this.player.paging = new PagingInfo(this.pageIndex, page_size, highestIndex);

    this.boxes = Array.from(new Array(page_size), (array, index) =>
      ({index: (this.pageIndex * page_size) +  1 + index}));
  }

  private _sequence: Sequence;
  private _unregisterHandlerFn: Function;
}