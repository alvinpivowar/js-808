import { Component, OnInit } from "@angular/core"
import { Player } from "../../models/Player"

@Component({
  selector: "js808-synthesizer",
  templateUrl: "./js808-synthesizer.component.html",
  styleUrls: ["./js808-synthesizer.component.scss"]
})

export class Js808SynthesizerComponent implements OnInit {

  public player: Player;

  constructor() {
    this.player = new Player();
  }

  ngOnInit() {
  }
}