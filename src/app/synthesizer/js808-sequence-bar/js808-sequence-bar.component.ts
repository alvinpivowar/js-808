import { Component, Input, OnInit } from "@angular/core"

import { Player } from "../../models/Player"
import { Sequence } from "../../models/Sequence"
import { SequenceService } from "../../services/sequence.service"

const componentName: string = "js808-sequence-bar";
@Component({
  selector: componentName,
  templateUrl: "./js808-sequence-bar.component.html",
  styleUrls: ["./js808-sequence-bar.component.scss"]
})

export class Js808SequenceBarComponent implements OnInit {
  public activeSequence: Sequence;

  @Input() player: Player;

  constructor(
    private sequenceService: SequenceService
  ) {}

  public onSequenceChange(sequenceName) {
    this.sequenceService.getSequenceByName(sequenceName).then(sequence => {
      this.activeSequence = sequence;
      this.player.sequence = this.activeSequence;
    });
  }

  ngOnInit() {
  }
}