import { Component, Input, OnDestroy, OnInit } from "@angular/core"

import { Part } from "../../models/Part"
import { Player, PlayerEvent, PlayerEventType } from "../../models/Player"

const component_name: string = "js808-parts-collection";
@Component({
  selector: component_name,
  templateUrl: "./js808-parts-collection.component.html",
  styleUrls: ["./js808-parts-collection.component.scss"]
})

export class Js808PartsCollectionComponent implements OnDestroy, OnInit {
  public parts: Array<Part>;

  @Input() player: Player;

  constructor() {}

  ngOnInit() {
    this._unregisterHandlerFn = this.player.registerEventHandler(component_name, this.handler.bind(this));
  }

  ngOnDestroy() {
    if (this._unregisterHandlerFn)
      this._unregisterHandlerFn();
  }

  private handler(event: PlayerEvent): Boolean {
    if (event.type === PlayerEventType.Sequence) {
      const sequence = event.data;

      this.parts = sequence.parts || [];
      return true;
    }

    return false;
  }

  private _unregisterHandlerFn: Function;
}