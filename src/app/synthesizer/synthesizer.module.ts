import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { FormsModule } from "@angular/forms"
import { HttpClientModule } from "@angular/common/http"

import { SequenceService } from "../services/sequence.service";

import { Js808ButtonBarComponent } from "./js808-button-bar/js808-button-bar.component"
import { Js808PartComponent } from "./js808-part/js808-part.component"
import { Js808PartsCollectionComponent } from "./js808-parts-collection/js808-parts-collection.component"
import { Js808PatternBarComponent } from "./js808-pattern-bar/js808-pattern-bar.component"
import { Js808PlayerControlsComponent } from "./js808-player-controls/js808-player-controls.component"
import { Js808SequenceBarComponent } from "./js808-sequence-bar/js808-sequence-bar.component"
import { Js808SequenceSelectorComponent } from "./js808-sequence-selector/js808-sequence-selector.component"
import { Js808SynthesizerComponent } from "./js808-synthesizer/js808-synthesizer.component"

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    Js808ButtonBarComponent,
    Js808PartComponent,
    Js808PartsCollectionComponent,
    Js808PatternBarComponent,
    Js808PlayerControlsComponent,
    Js808SequenceBarComponent,
    Js808SequenceSelectorComponent,
    Js808SynthesizerComponent
  ],
  providers: [
    SequenceService
  ],
  exports: [ Js808SynthesizerComponent ]
})
export class SynthesizerModule {}