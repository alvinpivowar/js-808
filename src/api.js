const express = require("express");
const router = express.Router();

let voiceData = [
  { name: "Closed Hat", color: "#FFD700" },
  { name: "Kick", color: "#87CEFA" },
  { name: "Open Hat", color: "#FFD700" },
  { name: "Snare", color: "#FFA07A" },
  { name: "Tom Tom", color: "#FFA07A" },
  { name: "Triangle", color: "#FFD700" }
];

let sequenceData = [
  {
    sequenceName: "Sequence1",
    authorName: "John Smith",
    createDateTime: "2018-01-01T09:00",
    version: 1,
    versionDateTime: "2018-01-01T12:00",

    timeSignature: { upperNumeral: 4, lowerNumeral: 4},
    beatsPerMinute: 128,
    
    parts: [
      {
        voice: voiceData[1],
        pattern: {
          steps: [
            { value: true },{},{},{},  { value: true },{},{},{},
            { value: true },{},{},{},  { value: true},{},{},{}
          ]
        }
      },

      {
        voice: voiceData[3],
        pattern: {
          steps: [
            {},{},{},{},  { value: true },{},{},{},
            {},{},{},{},  { value: true },{},{},{}
          ]
        }
      },

      {
        voice: voiceData[2],
        pattern: {
          steps: [
            {},{},{ value: true },{},  {},{},{ value: true },{},
            {},{},{ value: true },{},  {},{},{ value: true },{}
          ]
        }
      },

      {
        voice: voiceData[0],
        pattern: {
          steps: [
            { value: true },{},{},{},  { value: true },{},{},{},
            { value: true },{},{},{},  { value: true },{},{},{}
          ]
        }
      },
    ]
  },

  {
    sequenceName: "Empty, All Voices",
    authorName: "Mr. Smedley",
    createDateTime: "2018-01-02T09:00",
    version: 1,
    versionDateTime: "2018-01-02T12:00",

    timeSignature: { upperNumeral: 4, lowerNumeral: 4},
    beatsPerMinute: 60,

    parts: [
      { voice: voiceData[0] },
      { voice: voiceData[1] },
      { voice: voiceData[2] },
      { voice: voiceData[3] },
      { voice: voiceData[4] },
      { voice: voiceData[5] },
    ]
  },
  
  {
    sequenceName: "Monotany",
    authorName: "Mr. Boring",
    createDateTime: "2018-01-02T09:00",
    version: 1,
    versionDateTime: "2018-01-02T12:00",

    timeSignature: { upperNumeral: 2, lowerNumeral: 8},
    beatsPerMinute: 120,

    parts: [
      { voice: voiceData[3], pattern: { steps: [{value: true},{},{value: true},{},{value: true}, {},{value: true},{},{value: true},{}, {value: true},{},{value: true},{},{value: true}, {},{value: true},{},{value: true},{}]} },
      { voice: voiceData[5], pattern: { steps: [{},{value: true},{},{value: true},{}, {value: true},{},{value: true},{},{value: true}, {},{value: true},{},{value: true},{}, {value: true},{},{value: true},{},{value: true}]} },
    ]
  }
];


router.get("/sequences", (req, res, next) => {
  let result = sequenceData.map(sequence => ({
    sequenceName: sequence.sequenceName,
    authorName: sequence.authorName,
    createDateTime: sequence.createDateTime
  }));
  res.send(result);
});

router.get("/sequences/:sequenceName", (req, res, next) => {
  let result = sequenceData.find(sequence => sequence.sequenceName === req.params.sequenceName);
  res.send(result);
});

router.get("/voices", (req, res, next) => {
  res.send(voiceData);
})


module.exports = router;