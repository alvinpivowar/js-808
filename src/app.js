const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const express = require("express");
const path = require("path");
const morgan = require("morgan");
const serveFavicon = require("serve-favicon");

const api = require("./api");


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname)));
app.use(morgan("dev"));
app.use(serveFavicon(path.join(__dirname, "favicon.ico")));

app.use("/api", api);

app.use((req, res, next) => {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

const port = app.get("env") === "development" ? 3000 : 8080;
app.listen(port, () => console.log(`Node.js listening on port ${port}`));


module.exports = app;